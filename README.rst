Image Gallery
*************

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-gallery
  source venv-gallery/bin/activate
  # or
  python3 -m venv venv-gallery

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
