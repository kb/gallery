# -*- encoding: utf-8 -*-
from django.conf import settings
from django.urls import include, re_path
from django.conf.urls.static import static
from django.urls import path

from .views import DashView, HomeView, PostcardDetailView, SettingsView


urlpatterns = [
    re_path(r"^", view=include("login.urls")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^example/postcard/(?P<pk>\d+)/$",
        view=PostcardDetailView.as_view(),
        name="example.postcard.detail",
    ),
    re_path(r"^mail/", view=include("mail.urls")),
    re_path(r"^gallery/", view=include("gallery.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
