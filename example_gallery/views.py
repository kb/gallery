# -*- encoding: utf-8 -*-
from django.views.generic import DetailView, TemplateView

from base.view_utils import BaseMixin
from .models import Postcard


class DashView(BaseMixin, TemplateView):
    template_name = "example/dash.html"


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class PostcardDetailView(BaseMixin, DetailView):
    model = Postcard


class SettingsView(BaseMixin, TemplateView):
    template_name = "example/settings.html"
