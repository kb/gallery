# -*- encoding: utf-8 -*-
import factory

from example_gallery.models import Postcard, PostcardImage


class PostcardImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PostcardImage


class PostcardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Postcard
