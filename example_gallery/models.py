# -*- encoding: utf-8 -*-
from django.db import models
from django.urls import reverse

from gallery.models import Image, Wizard, WizardModelMixin


class Postcard(models.Model, WizardModelMixin):
    picture = models.ForeignKey(
        Image, related_name="+", blank=True, null=True, on_delete=models.CASCADE
    )
    slideshow = models.ManyToManyField(
        Image, related_name="slideshow", through="PostcardImage"
    )

    def get_design_url(self):
        return reverse("example.postcard.detail", args=[self.pk])

    def get_absolute_url(self):
        return reverse("example.postcard.detail", args=[self.pk])

    def ordered_slideshow(self):
        return self.slideshow.through.objects.filter(content=self)

    def set_pending_edit(self):
        return True

    @property
    def wizard_fields(self):
        return [
            Wizard("picture", Wizard.IMAGE, Wizard.SINGLE),
            Wizard("slideshow", Wizard.IMAGE, Wizard.MULTI),
        ]


class PostcardImage(models.Model):
    """Slideshow images for the postcard.

    This is the model that is used to govern the many-to-many relationship
    between ``Postcard`` and ``Image``.

    https://docs.djangoproject.com/en/1.8/topics/db/models/#extra-fields-on-many-to-many-relationships

    """

    content = models.ForeignKey(Postcard, on_delete=models.CASCADE)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    order = models.IntegerField()

    class Meta:
        ordering = ["order"]
        verbose_name = "Postcard Image"
        verbose_name_plural = "Postcard Images"
