# -*- encoding: utf-8 -*-
from django import forms

from django.utils.html import format_html
from easy_thumbnails.files import get_thumbnailer

from base.form_utils import (
    FileDropInput,
    RequiredFieldForm,
    set_widget_required,
)
from .models import Image, ImageCategory


def _label_from_instance(obj):
    """The label is the image."""
    thumbnailer = get_thumbnailer(obj.image)
    thumbnail_options = {"crop": True, "size": (100, 0)}
    thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
    html = """<span title="Filename: {}&#10;Dimensions: {}x{}{}">
        {}
        <br>
        <img src="{}" />
        </span>
    """
    return format_html(
        html.format(
            obj.original_file_name,
            obj.image.width if obj.image else "?",
            obj.image.height if obj.image else "?",
            ("&#10;Tags: " + (", ".join(obj.tags.names())))
            if obj.tags.names()
            else "",
            obj.title,
            thumbnail.url,
        )
    )


def _label_from_many_to_many_instance(obj):
    """The label is the image."""
    thumbnailer = get_thumbnailer(obj.image.image)
    thumbnail_options = {"crop": True, "size": (100, 0)}
    thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
    return format_html(
        '{}. {}<br><img src="{}" />'.format(
            obj.order, obj.image.title, thumbnail.url
        )
    )


class ImageModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    """The label is the image."""

    def label_from_instance(self, obj):
        return _label_from_instance(obj)


class ImageCategoryEmptyForm(forms.ModelForm):
    class Meta:
        model = ImageCategory
        fields = ()


class ImageCategoryForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = ImageCategory
        fields = ("name",)


class ImageListDeleteForm(forms.Form):
    """List of images (so the user can delete them)."""

    images = ImageModelMultipleChoiceField(
        queryset=Image.objects.images(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )

    class Meta:
        fields = ("images",)


class ImageModelChoiceField(forms.ModelChoiceField):
    """The label is the image."""

    def label_from_instance(self, obj):
        return _label_from_instance(obj)


class ImageMultiSelectForm(forms.Form):
    """List of images (for the form wizard)."""

    images = ImageModelMultipleChoiceField(
        queryset=Image.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, *args, **kwargs):
        image_queryset = kwargs.pop("image_queryset")
        super().__init__(*args, **kwargs)
        images = self.fields["images"]
        images.queryset = Image.objects.filter(pk__in=image_queryset)

    class Meta:
        fields = ("images",)


class ImageUpdateForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        title = self.fields["title"]
        set_widget_required(title)
        category = self.fields["category"]
        category.queryset = ImageCategory.objects.categories()
        for name in ["category", "tags", "title"]:
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = Image
        fields = ("title", "category", "tags")


class ManyToManyMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return _label_from_many_to_many_instance(obj)


class EmptyForm(forms.Form):
    class Meta:
        fields = ()


class ImageForm(forms.ModelForm):
    """Allow the user to upload an image (for the form wizard)."""

    add_to_library = forms.BooleanField(
        help_text="tick this box to add the image to the library",
        initial=True,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        in_library = kwargs.pop("in_library", None)
        super().__init__(*args, **kwargs)
        for field_name in ["image", "title"]:
            field = self.fields[field_name]
            set_widget_required(field)
        for name in ("category", "image", "tags", "title"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-2-3"})
        category = self.fields["category"]
        category.queryset = ImageCategory.objects.categories()
        if in_library:
            del self.fields["add_to_library"]

    class Meta:
        model = Image
        fields = ("image", "title", "category", "add_to_library", "tags")
        widgets = {"image": FileDropInput()}


class ImageListForm(forms.Form):
    """List of images (for the form wizard)."""

    images = ImageModelChoiceField(
        queryset=Image.objects.none(),
        empty_label=None,
        widget=forms.RadioSelect,
    )

    def __init__(self, *args, **kwargs):
        image_queryset = kwargs.pop("image_queryset")
        super().__init__(*args, **kwargs)
        images = self.fields["images"]
        images.queryset = image_queryset

    class Meta:
        model = Image
        fields = ("images",)


class ImageSelectForm(forms.Form):
    """List of current images in the slideshow."""

    # Note: The ``queryset`` will not contain ``Image`` records.
    many_to_many = ManyToManyMultipleChoiceField(
        queryset=Image.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, *args, **kwargs):
        qs_many_to_many = kwargs.pop("many_to_many")
        super().__init__(*args, **kwargs)
        many_to_many = self.fields["many_to_many"]
        many_to_many.queryset = qs_many_to_many.order_by("order")
        # tick every link - so the user can untick the ones they want to remove
        initial = {item.pk: True for item in qs_many_to_many}
        many_to_many.initial = initial
