# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    ImageCategoryCreateView,
    ImageCategoryDeleteView,
    ImageCategoryListView,
    ImageCategoryUpdateView,
    ImageCreateView,
    ImageListView,
    ImageListDeleteView,
    ImageUpdateView,
    WizardImageChoose,
    WizardImageOption,
    WizardImageOrder,
    WizardImageRemove,
    WizardImageSelect,
    WizardImageUpload,
)


urlpatterns = [
    # image category
    re_path(
        r"^image/category/$",
        view=ImageCategoryListView.as_view(),
        name="gallery.image.category.list",
    ),
    re_path(
        r"^image/category/create/$",
        view=ImageCategoryCreateView.as_view(),
        name="gallery.image.category.create",
    ),
    re_path(
        r"^image/category/(?P<pk>\d+)/update/$",
        view=ImageCategoryUpdateView.as_view(),
        name="gallery.image.category.update",
    ),
    re_path(
        r"^image/category/(?P<pk>\d+)/delete/$",
        view=ImageCategoryDeleteView.as_view(),
        name="gallery.image.category.delete",
    ),
    # image
    re_path(
        r"^image/$",
        view=ImageListView.as_view(),
        name="gallery.image.list",
    ),
    re_path(
        r"^image/create/$",
        view=ImageCreateView.as_view(),
        name="gallery.image.create",
    ),
    re_path(
        r"^image/delete/$",
        view=ImageListDeleteView.as_view(),
        name="gallery.image.list.delete",
    ),
    re_path(
        r"^image/(?P<pk>\d+)/update/$",
        view=ImageUpdateView.as_view(),
        name="gallery.image.update",
    ),
    # image wizard
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/choose/$",
        view=WizardImageChoose.as_view(),
        name="gallery.wizard.image.choose",
    ),
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/(?P<category>[-\w\d]+)/choose/$",
        view=WizardImageChoose.as_view(),
        name="gallery.wizard.image.choose",
    ),
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/order/$",
        view=WizardImageOrder.as_view(),
        name="gallery.wizard.image.order",
    ),
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/option/$",
        view=WizardImageOption.as_view(),
        name="gallery.wizard.image.option",
    ),
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/remove/$",
        view=WizardImageRemove.as_view(),
        name="gallery.wizard.image.remove",
    ),
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/select/$",
        view=WizardImageSelect.as_view(),
        name="gallery.wizard.image.select",
    ),
    re_path(
        r"^image/(?P<content>\d+)/(?P<pk>\d+)/(?P<field>[-\w\d]+)/"
        r"(?P<type>[-\w\d]+)/upload/$",
        view=WizardImageUpload.as_view(),
        name="gallery.wizard.image.upload",
    ),
]
