# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from gallery.tests.factories import ImageCategoryFactory, ImageFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_image(perm_check):
    """Image library."""
    ImageFactory()
    url = reverse("gallery.image.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_image_category(perm_check):
    perm_check.staff(reverse("gallery.image.category.list"))


@pytest.mark.django_db
def test_image_category_create(perm_check):
    perm_check.staff(reverse("gallery.image.category.create"))


@pytest.mark.django_db
def test_image_category_delete(perm_check):
    obj = ImageCategoryFactory()
    perm_check.staff(reverse("gallery.image.category.delete", args=[obj.pk]))


@pytest.mark.django_db
def test_image_category_update(perm_check):
    obj = ImageCategoryFactory()
    perm_check.staff(reverse("gallery.image.category.update", args=[obj.pk]))


@pytest.mark.django_db
def test_image_create(perm_check):
    url = reverse("gallery.image.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_image_delete(perm_check):
    ImageFactory()
    url = reverse("gallery.image.list.delete")
    perm_check.staff(url)


@pytest.mark.django_db
def test_image_update(perm_check):
    image = ImageFactory()
    url = reverse("gallery.image.update", args=[image.pk])
    perm_check.staff(url)
