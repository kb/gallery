# -*- encoding: utf-8 -*-
from gallery.models import Image, ImageCategory
import factory


class ImageCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ImageCategory

    @factory.sequence
    def name(n):
        return "name_{}".format(n)


class ImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Image

    image = factory.django.ImageField()
