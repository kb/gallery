# -*- encoding: utf-8 -*-
import pytest

from gallery.templatetags.gallery_tags import calc_hdpi_size, calc_thumb_size


@pytest.mark.django_db
def test_calc_thumb_size():
    assert calc_thumb_size("800", 2) == "400x0"


@pytest.mark.django_db
def test_calc_hdpi_size():
    assert calc_hdpi_size("580x350", 2) == "1160x700"
