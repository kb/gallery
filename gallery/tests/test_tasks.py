# -*- encoding: utf-8 -*-
import pytest

from gallery.tasks import thumbnail_image
from gallery.tests.factories import ImageFactory


@pytest.mark.django_db
def test_thumbnail_image():
    image = ImageFactory()
    thumbnail_image(image.pk)
