# -*- encoding: utf-8 -*-
import pytest
import os
import tempfile

from PIL import Image as PILImage
from django.core.files import File

from gallery.models import Image
from gallery.tests.factories import ImageCategoryFactory, ImageFactory


@pytest.mark.django_db
def test_image_factory():
    ImageFactory()


@pytest.mark.django_db
def test_image_str():
    str(ImageFactory())


@pytest.mark.django_db
def test_tags_by_category():
    category = ImageCategoryFactory()
    i1 = ImageFactory(category=category)
    i1.tags.add("a")
    i2 = ImageFactory(category=category)
    i2.tags.add("b")
    i3 = ImageFactory(category=category)
    i3.tags.add("b")
    qs = Image.objects.tags_by_category(category)
    assert [("b", 2), ("a", 1)] == [(x.slug, x.num_tags) for x in qs]


@pytest.mark.django_db
def test_image_exists():
    color = (0, 160, 150)
    size = (64, 64)
    im = PILImage.new("RGB", size, color)
    im_file = tempfile.NamedTemporaryFile(suffix=".png")
    im.save(im_file)
    im_file.seek(0)
    image = ImageFactory()
    image.image.save("test_image.png", File(im_file))
    assert image.file_exists
    # tidy
    os.remove(image.image.file.name)
    assert not image.file_exists


@pytest.mark.django_db
def test_image_size():
    color = (0, 160, 150)
    size = (100, 200)
    im = PILImage.new("RGB", size, color)
    im_file = tempfile.NamedTemporaryFile(suffix=".png")
    im.save(im_file)
    im_file.seek(0)
    image = ImageFactory()
    image.image.save("test_image.png", File(im_file))
    width, height = image.image_size
    assert width == 100 and height == 200
    # tidy
    os.remove(image.image.file.name)
    assert not image.file_exists
