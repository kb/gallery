# -*- encoding: utf-8 -*-
import pytest

from gallery.models import ImageCategory
from gallery.tests.factories import ImageCategoryFactory
from gallery.tests.factories import ImageFactory


@pytest.mark.django_db
def test_categories():
    c1 = ImageCategoryFactory(name="a")
    ImageCategoryFactory(name="b", deleted=True)
    c3 = ImageCategoryFactory(name="c")
    assert [c1.slug, c3.slug] == [
        c.slug for c in ImageCategory.objects.categories()
    ]


@pytest.mark.django_db
def test_in_use():
    c = ImageCategoryFactory()
    ImageFactory(category=c)
    assert c.in_use is True


@pytest.mark.django_db
def test_in_use_deleted():
    c = ImageCategoryFactory()
    ImageFactory(category=c, deleted=True)
    assert c.in_use is False


@pytest.mark.django_db
def test_in_use_not():
    c = ImageCategoryFactory()
    assert c.in_use is False
