# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import transaction
from django.db.models import Max
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import (
    CreateView,
    FormView,
    ListView,
    TemplateView,
    UpdateView,
)

from base.view_utils import BaseMixin, RedirectNextMixin
from .forms import (
    EmptyForm,
    ImageCategoryEmptyForm,
    ImageCategoryForm,
    ImageForm,
    ImageListForm,
    ImageListDeleteForm,
    ImageMultiSelectForm,
    ImageSelectForm,
    ImageUpdateForm,
)
from .models import Image, ImageCategory, GalleryError, Wizard
from .tasks import thumbnail_image


class ImageCategoryCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = ImageCategoryForm
    model = ImageCategory

    def get_success_url(self):
        return reverse("gallery.image.category.list")


class ImageCategoryDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ImageCategoryEmptyForm
    model = ImageCategory
    template_name = "gallery/imagecategory_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.in_use:
            raise GalleryError(
                "Cannot delete an image category which is "
                "in use: '{}'".format(self.object.slug)
            )
        self.object.deleted = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("gallery.image.category.list")


class ImageCategoryListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = ImageCategory

    def get_queryset(self):
        return ImageCategory.objects.categories()


class ImageCategoryUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ImageCategoryForm
    model = ImageCategory

    def get_success_url(self):
        return reverse("gallery.image.category.list")


class ImageListDeleteView(LoginRequiredMixin, StaffuserRequiredMixin, FormView):
    """Mark images (in the library) as deleted.

    Images will still be attached to content objects, but will not display in
    the library (image wizard).

    """

    form_class = ImageListDeleteForm
    template_name = "gallery/image_list_delete.html"

    def form_valid(self, form):
        images = form.cleaned_data["images"]
        for image in images:
            image.set_deleted()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("gallery.image.list")


class ImageMaintenanceMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"url_option": reverse("gallery.image.list")})
        return context


class ImageCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ImageMaintenanceMixin,
    CreateView,
):
    form_class = ImageForm
    template_name = "gallery/wizard_image_upload.html"
    model = Image

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.user = self.request.user
            self.object.save()
        transaction.on_commit(lambda: thumbnail_image.send(self.object.pk))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("gallery.image.list")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"in_library": True})
        return kwargs


class ImageListView(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    def get_queryset(self):
        return Image.objects.images()


class ImageUpdateView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    form_class = ImageUpdateForm
    model = Image

    def get_success_url(self):
        return reverse("gallery.image.list")


class WizardMixin(RedirectNextMixin):
    def _content_obj(self):
        content_type_pk = self.kwargs["content"]
        pk = self.kwargs["pk"]
        content_type = ContentType.objects.get(pk=content_type_pk)
        content_model = content_type.model_class()
        return content_model.objects.get(pk=pk)

    def _field_name(self):
        return self.kwargs["field"]

    def _get_field(self):
        content_obj = self._content_obj()
        field_name = self._link_field_name(content_obj)
        return getattr(content_obj, field_name)

    def _get_many_to_many(self):
        link_type = self._link_type()
        if link_type == Wizard.MULTI:
            content_obj = self._content_obj()
            field = self._get_field()
            return field.through.objects.filter(content=content_obj)
        else:
            raise GalleryError(
                "Cannot '_get_many_to_many' for 'link_type': '{}'".format(
                    link_type
                )
            )

    def _kwargs(self):
        content_type_pk = self.kwargs["content"]
        wizard_type = self.kwargs["type"]
        content_obj = self._content_obj()
        return {
            "content": content_type_pk,
            "pk": content_obj.pk,
            "field": self._field_name(),
            "type": wizard_type,
        }

    def _link_field_name(self, content_obj):
        """Assign the link to the field with this name."""
        field_name = self.kwargs["field"]
        if hasattr(content_obj, field_name):
            return field_name
        else:
            raise GalleryError(
                "Content object '{}' does not have a field "
                "named '{}'".format(content_obj.__class__.__name__, field_name)
            )

    def _link_type(self):
        """Is this a 'single' or a 'multi' link?"""
        return self.kwargs["type"]

    def _page_design_url(self, content_obj):
        """Get the page design URL.

        This method tries to get the design URL from the content object first
        so the image wizard is able to work with non-block models.

        """
        result = self.request.GET.get(REDIRECT_FIELD_NAME)
        if not result:
            result = self.request.POST.get(REDIRECT_FIELD_NAME)
        if not result:
            try:
                result = content_obj.get_design_url()
            except AttributeError:
                result = content_obj.block.page_section.page.get_design_url()
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        next_url = self.request.GET.get(REDIRECT_FIELD_NAME)
        context.update(dict(next_url=next_url))
        return context


class WizardImageMixin(WizardMixin):
    def _get_image(self):
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            field = self._get_field()
            return field
        else:
            raise GalleryError(
                "Cannot '_get_image' for 'link_type': '{}'".format(link_type)
            )

    def _update_image(self, content_obj, image):
        field_name = self._link_field_name(content_obj)
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            setattr(content_obj, field_name, image)
        elif link_type == Wizard.MULTI:
            field = self._get_field()
            class_many_to_many = field.through
            result = class_many_to_many.objects.filter(
                content=content_obj
            ).aggregate(Max("order"))
            order = result.get("order__max") or 0
            order = order + 1
            obj = class_many_to_many(
                content=content_obj, image=image, order=order
            )
            obj.save()
        else:
            raise GalleryError("Unknown 'link_type': '{}'".format(link_type))
        content_obj.set_pending_edit()
        content_obj.save()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kwargs = self._kwargs()
        # categories
        categories = []
        for category in ImageCategory.objects.categories():
            kw = kwargs.copy()
            kw.update({"category": category.slug})
            url = reverse("gallery.wizard.image.choose", kwargs=kw)
            categories.append(dict(name=category.name, url=url))
        content_obj = self._content_obj()
        context.update(
            dict(
                categories=categories,
                field_name=self._field_name(),
                object=content_obj,
                tags=Image.tags.all(),
                url_page_design=self._page_design_url(content_obj),
                url_choose=reverse(
                    "gallery.wizard.image.choose", kwargs=kwargs
                ),
                url_option=reverse(
                    "gallery.wizard.image.option", kwargs=kwargs
                ),
                url_order=reverse("gallery.wizard.image.order", kwargs=kwargs),
                url_remove=reverse(
                    "gallery.wizard.image.remove", kwargs=kwargs
                ),
                url_select=reverse(
                    "gallery.wizard.image.select", kwargs=kwargs
                ),
                url_upload=reverse(
                    "gallery.wizard.image.upload", kwargs=kwargs
                ),
            )
        )
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            context.update(dict(image=self._get_image()))
        elif link_type == Wizard.MULTI:
            context.update(dict(many_to_many=self._get_many_to_many()))
        else:
            raise GalleryError("Unknown 'link_type': '{}'".format(link_type))
        return context


class WizardMoveMixin:
    """Mixin for moving links or images up and down."""

    form_class = EmptyForm

    def _move_up_down(self, up, down):
        """Move up or down.

        .. note:: This moving logic is also used by ``_move_block_up_down`` in
                  the ``PageSection`` model

        """
        pk = int(up) if up else int(down)
        idx = None
        ordered = []
        many_to_many = self._get_many_to_many()
        for count, item in enumerate(many_to_many):
            if item.pk == pk:
                idx = count
            ordered.append(item.pk)
            count = count + 1
        if idx is None:
            raise GalleryError("Cannot find item {} in {}".format(pk, ordered))
        if down:
            if idx == len(ordered) - 1:
                raise GalleryError("Cannot move the last item down")
            ordered[idx], ordered[idx + 1] = ordered[idx + 1], ordered[idx]
        elif up:  # up
            if idx == 0:
                raise GalleryError("Cannot move the first item up")
            ordered[idx], ordered[idx - 1] = ordered[idx - 1], ordered[idx]
        else:
            raise GalleryError("No 'up' or 'down' (why?)")
        content_obj = self._content_obj()
        field = self._get_field()
        with transaction.atomic():
            for order, pk in enumerate(ordered, start=1):
                obj = field.through.objects.get(pk=pk, content=content_obj)
                obj.order = order
                obj.save()
            content_obj.set_pending_edit()
            content_obj.save()

    def form_valid(self, form):
        up = self.request.POST.get("up")
        down = self.request.POST.get("down")
        if up or down:
            self._move_up_down(up, down)
        return HttpResponseRedirect(self.get_success_url())


class WizardImageChoose(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardImageMixin, FormView
):
    template_name = "gallery/wizard_image_choose.html"

    def _category_slug(self):
        return self.kwargs.get("category")

    def _image_queryset(self):
        category_slug = self._category_slug()
        tag = self._tag()
        qs = Image.objects.images()
        if category_slug:
            qs = qs.filter(category__slug=category_slug)
        if tag:
            qs = qs.filter(tags__slug__in=[tag])
        return qs

    def _paginator(self, qs):
        page = self.request.GET.get("page")
        paginator = Paginator(qs, 16)
        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            page_obj = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results
            page_obj = paginator.page(paginator.num_pages)
        return page_obj

    def _tag(self):
        return self.request.GET.get("tag")

    def _update_images_many_to_many(self, images):
        content_obj = self._content_obj()
        field = self._get_field()
        with transaction.atomic():
            field = self._get_field()
            class_many_to_many = field.through
            result = class_many_to_many.objects.filter(
                content=content_obj
            ).aggregate(Max("order"))
            order = result.get("order__max") or 0
            for image in images:
                order = order + 1
                obj = class_many_to_many(
                    content=content_obj, image=image, order=order
                )
                obj.save()
            content_obj.set_pending_edit()
            content_obj.save()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = None
        category_slug = self._category_slug()
        if category_slug:
            category = ImageCategory.objects.get(slug=category_slug)
            tags = Image.objects.tags_by_category(category)
        else:
            tags = Image.tags.most_common()
        try:
            page_obj = self.page_obj
        except AttributeError:
            page_obj = None
        context.update(
            dict(
                category=category,
                is_paginated=page_obj.has_other_pages() if page_obj else None,
                page_obj=page_obj,
                tag=self._tag(),
                tags=tags,
            )
        )
        return context

    def get_form_class(self):
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            return ImageListForm
        elif link_type == Wizard.MULTI:
            return ImageMultiSelectForm
        else:
            raise GalleryError("Unknown 'link_type': '{}'".format(link_type))

    def get_form_kwargs(self):
        """kwargs that will be passed to the __init__ of your form.

        We only paginate the queryset for the form on a ``GET``.  A ``POST``
        needs access to all images for validation.

        The paginator slices the queryset.  If this queryset is passed to the
        form validation, we get a ``Cannot filter a query once a slice has been
        taken`` error message.  To overcome this we create a new queryset:
        http://stackoverflow.com/questions/3470111/cannot-filter-a-query-once-a-slice-has-been-taken

        """
        kwargs = super().get_form_kwargs()
        qs = self._image_queryset()
        if not self.request.method == "POST":
            self.page_obj = self._paginator(qs)
            qs = Image.objects.filter(pk__in=self.page_obj.object_list)
        kwargs.update(dict(image_queryset=qs))
        return kwargs

    def form_valid(self, form):
        images = form.cleaned_data["images"]
        content_obj = self._content_obj()
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            self._update_image(content_obj, images)
            url = self._page_design_url(content_obj)
        elif link_type == Wizard.MULTI:
            self._update_images_many_to_many(images)
            url = reverse("gallery.wizard.image.option", kwargs=self._kwargs())
        else:
            raise GalleryError("Unknown 'link_type': '{}'".format(link_type))
        return HttpResponseRedirect(url)


class WizardImageOption(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardImageMixin, TemplateView
):
    template_name = "gallery/wizard_image_option.html"


class WizardImageOrder(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    WizardMoveMixin,
    WizardImageMixin,
    FormView,
):
    """Set the order of multiple images."""

    template_name = "gallery/wizard_image_order.html"

    def get_success_url(self):
        return reverse("gallery.wizard.image.order", kwargs=self._kwargs())


class WizardImageRemove(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardImageMixin, FormView
):
    form_class = EmptyForm
    template_name = "gallery/wizard_image_remove.html"

    def _content_obj(self):
        content_type_pk = self.kwargs["content"]
        pk = self.kwargs["pk"]
        content_type = ContentType.objects.get(pk=content_type_pk)
        content_model = content_type.model_class()
        return content_model.objects.get(pk=pk)

    def form_valid(self, form):
        """Set the image on the content object to ``None`` (remove it)."""
        content_obj = self._content_obj()
        self._update_image(content_obj, None)
        return HttpResponseRedirect(self._page_design_url(content_obj))

    def get_context_data(self, **kwargs):
        """Return the current image in the context, so we can display it."""
        context = super().get_context_data(**kwargs)
        field_name = self.kwargs["field"]
        content_object = self._content_obj()
        context.update(dict(image=getattr(content_object, field_name)))
        return context

    def get_object(self):
        return self._content_obj()


class WizardImageSelect(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardImageMixin, FormView
):
    """List the current images in the slideshow and allow the user to remove.

    Allow the user to de-select any of the images.

    """

    form_class = ImageSelectForm
    template_name = "gallery/wizard_image_select.html"

    def _update_many_to_many(self, many_to_many):
        content_obj = self._content_obj()
        field = self._get_field()
        with transaction.atomic():
            field = self._get_field()
            field.clear()
            class_many_to_many = field.through
            order = 0
            for item in many_to_many:
                order = order + 1
                obj = class_many_to_many(
                    content=content_obj, image=item.image, order=order
                )
                obj.save()
            content_obj.set_pending_edit()
            content_obj.save()

    def form_valid(self, form):
        many_to_many = form.cleaned_data["many_to_many"]
        self._update_many_to_many(many_to_many)
        return HttpResponseRedirect(
            reverse("gallery.wizard.image.option", kwargs=self._kwargs())
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(many_to_many=self._get_many_to_many()))
        return kwargs


class WizardImageUpload(
    LoginRequiredMixin, StaffuserRequiredMixin, WizardImageMixin, CreateView
):
    form_class = ImageForm
    template_name = "gallery/wizard_image_upload.html"

    def form_valid(self, form):
        content_obj = self._content_obj()
        with transaction.atomic():
            self.object = form.save()
            self.object.user = self.request.user
            self.object.save()
            self._update_image(content_obj, self.object)
        transaction.on_commit(lambda: thumbnail_image.send(self.object.pk))
        link_type = self._link_type()
        if link_type == Wizard.SINGLE:
            url = self._page_design_url(content_obj)
        elif link_type == Wizard.MULTI:
            url = reverse("gallery.wizard.image.option", kwargs=self._kwargs())
        return HttpResponseRedirect(url)
