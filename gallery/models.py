# -*- encoding: utf-8 -*-
import os

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Count
from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from reversion import revisions as reversion
from taggit.models import Tag
from taggit.managers import TaggableManager

from base.model_utils import TimeStampedModel


class GalleryError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class ImageCategoryManager(models.Manager):
    def create_category(self, name):
        obj = self.model(name=name)
        obj.save()
        return obj

    def categories(self):
        return self.model.objects.all().exclude(deleted=True).order_by("slug")

    def init_category(self, name):
        try:
            obj = self.model.objects.get(name=name)
        except self.model.DoesNotExist:
            obj = self.create_category(name)
        return obj


class ImageCategory(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(max_length=100, unique=True, populate_from=("name",))
    deleted = models.BooleanField(default=False)
    objects = ImageCategoryManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Image Category"
        verbose_name_plural = "Image Categories"

    def __str__(self):
        return "{}".format(self.name)

    @property
    def in_use(self):
        images = Image.objects.filter(category=self, deleted=False)
        return images.count() > 0


reversion.register(ImageCategory)


class ImageManager(models.Manager):
    def images(self):
        return (
            self.model.objects.all()
            .exclude(deleted=True)
            .order_by("category__slug", "title")
        )

    def tags_by_category(self, category):
        return (
            Tag.objects.filter(image_tags__category__slug=category.slug)
            .annotate(num_tags=Count("pk"))
            .order_by("-num_tags")
        )


class Image(TimeStampedModel):
    """An image *gallery*, used with the 'ImageWizard'.

    For more information, see ``1011-generic-carousel/wip.rst``

    .. note:: Images are NOT links... so we don't use the ``LinkWizard``.
              We need a new wizard for images.

    .. note:: Image wizard...  For now... display all the images as little
              thumbnails with tick boxes for multi-selection and radio buttons
              for single selection.

    """

    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="link/image")
    original_file_name = models.CharField(max_length=100)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
        help_text="User who uploaded the image",
    )
    deleted = models.BooleanField(default=False)
    category = models.ForeignKey(
        ImageCategory, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = ImageManager()
    tags = TaggableManager(blank=True, related_name="image_tags")

    class Meta:
        verbose_name = "Link Image"
        verbose_name_plural = "Link Images"

    def __str__(self):
        return "{}. {}".format(self.pk, self.title)

    def save(self, *args, **kwargs):
        """Save the original file name."""
        self.original_file_name = os.path.basename(self.image.name)
        # Call the "real" save() method.
        super().save(*args, **kwargs)

    def set_deleted(self):
        self.deleted = True
        self.save()

    @property
    def file_exists(self):
        if self.image and self.image.storage:
            return self.image.storage.exists(self.image.file.name)
        return false

    @property
    def image_size(self):
        if self.file_exists:
            from PIL import Image as PILImage

            image = PILImage.open(self.image.file.name)
            size = image.size
            image.close()
            return size
        return (
            None,
            None,
        )


reversion.register(Image)


class Wizard:
    # 'wizard_type'
    IMAGE = "image"
    LINK = "link"

    # 'link_type'
    MULTI = "multi"
    SINGLE = "single"

    def __init__(self, field_name, wizard_type, link_type):
        self.field_name = field_name
        self.wizard_type = wizard_type
        self.link_type = link_type

    @property
    def css_class(self):
        result = ""
        if self.wizard_type == self.IMAGE:
            result = "fa fa-image"
        elif self.wizard_type == self.LINK:
            result = "fa fa-globe"
        else:
            raise GalleryError(
                "Unknown wizard type: '{}'".format(self.wizard_type)
            )
        return result

    @property
    def url_name(self):
        result = ""
        if self.wizard_type == self.IMAGE:
            result = "gallery.wizard.image.option"
        elif self.wizard_type == self.LINK:
            result = "gallery.wizard.link.option"
        else:
            raise GalleryError(
                "Unknown wizard type: '{}'".format(self.wizard_type)
            )
        return result


class WizardModelMixin:
    @property
    def wizard_urls(self):
        """Return the URLs for the image and link wizards."""
        result = []
        if hasattr(self, "wizard_fields"):
            for item in self.wizard_fields:
                content_type = ContentType.objects.get_for_model(self)
                result.append(
                    {
                        "caption": item.field_name.title().replace("_", " "),
                        "class": item.css_class,
                        "field_name": item.field_name,
                        "url": reverse(
                            item.url_name,
                            kwargs={
                                "content": content_type.pk,
                                "pk": self.pk,
                                "field": item.field_name,
                                "type": item.link_type,
                            },
                        ),
                    }
                )
        return result
