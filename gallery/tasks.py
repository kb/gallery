# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from easy_thumbnails.files import generate_all_aliases
from .models import Image


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def thumbnail_image(image_pk):
    logger.info("gallery, 'thumbnail_image' {}".format(image_pk))
    try:
        img = Image.objects.get(pk=image_pk)
    except Image.DoesNotExist:
        logger.info("gallery, 'thumbnail_image' image not found")
        img = None
    if img:
        logger.info("gallery, 'generate_all_aliases' {}".format(img.pk))
        generate_all_aliases(img.image, include_global=True)
        logger.info("gallery, 'generate_all_aliases' Complete")
