# -*- encoding: utf-8 -*-
import math
from django import template

register = template.Library()


@register.filter(name="calc_thumb_size")
def calc_thumb_size(value, divisor):
    try:
        v = int(value)
        d = int(divisor)
        return "{}x0".format(math.floor(v / d))
    except ValueError as e:
        # default to a useful size
        return "800x0"


@register.filter(name="calc_hdpi_size")
def calc_hdpi_size(value, multiplier):
    try:
        vals = value.split("x")
        w = int(vals[0]) * multiplier
        if len(vals) > 1:
            h = int(vals[1]) * multiplier
        else:
            h = 0
        return "{}x{}".format(w, h)
    except ValueError as e:
        # default to a 2x standard thumb size
        return "1600x0"
